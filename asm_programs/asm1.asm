SECTION .data   ;initialized data

msg: db "Hello world, this is assembly" ,10,0 ;this is out message, 10 makes us return, 0 ends the program

;printf
SECTION .text   ;asm code

extern printf ;we are bringing in printf from the outside
global main ;we have a label called main we will let this be available outside of our program

main:
    push ebp ; extended base pointer register
    mov ebp, esp ;setting up our stack frame by moving it to the base pointer
    
    push msg
    call printf

    mov esp, ebp ;
    pop ebp
    ret

SECTION .bss    ;unititalized code we don't need it in this program
