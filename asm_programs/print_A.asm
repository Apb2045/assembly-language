;assume cs:cseg, ds:cseg
;cseg segment


    org 100h

start:
    mov ax, 0B800h
    mov es, ax
    mov byte es:[0], 'A'
    int 20h

;cseg ends
end start
