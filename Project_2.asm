3Ch create;slow funciton
3Dh open;slow function
3Eh close
3Fh read
40h write
42h seek

;trick function called duplicate handle in assembly language. 
;you want to compare if file has two of the same halves
;put one hanldle in the beginning and one in the middle and see if there at the same thing
;for us we use duplcate handle to close the 2nd handle

;in unixh there are fuinctions creat open
;in c there are functions fopen


ah = 3ch create
ds:ds -> filename,o-terminated
cx=0 attributes
return: carry, if success ax=handle

;syntax for the second function

ah=3Dh open
ds:dx -> filename
al =     0-read ;how we are going to interact with the file
         1-write
         2-both
return: same as 3Ch

inname db "infile.dat", 0;
outname db "c:\work\output.data",0
inh dw ?
outh dw ?
b db N dup(?)

------------------------------
    mov ah, 3Dh
    mov dx, offset inname
    sub al,al ;read
    int 21h
    jc error
    mov inh,ax
    _____
    mov ah,3ch
    mov dx, offset outname
    sub cx,cx
    int 21h
    jc error
    mov outh,ax
---------------------------
ah=3Eh;close
bx=handle ;if not a valid handle should terminate

---------------------------
mov ah,3Eh
mov bx, inh
int 21h
mov ah,3Eh
mov bx,outh
int 21h
;close out the input files because its the polite thing to do

;one more thing about close function
mov ah, 3Eh
sub bx,bx
int 21h
;what would happen here?
;be careful dont supply random handle 0

ah=3Fh/40h
ds:dx -> data
cx= # of bytes
bx= handle
return: ax = #of bytes read/written
;write and fwrite functions tell the number of bytes read

again:

move ah,3Fh
mov bx,inh
mov cx,1
mov dx,offset b
int 21h
or ax,ax ;checks if the program actually produces byts 
jz done

mov ah, 40h ;make this mov cx, ax
mov bx, outh
mov cx, 1 ;make this mov ah,40h
mov dx, offset b
int 21h
jmp again

done:

;in a real program we would want to check if the function succeeds
;in a large program we would want to put an extra check to make sure handle is set up properly
;in a large program check if there are enough bytes left to write to 
;this program will be incredably slow, the reason is becase the professors code is not correc
;we should have a larger buffer 




;this is what we are trying to do the whole time.
c>mycopy infile outfile 

............................................................................................

;in the middle of the loop we read b...

read b

mov al, b
cmp al, 'a'
jl skip
cmp al, 'z'
jg skip
sub al, al
skip: mov b, al

write b 

;one thing we are missing is reading command line arguments 
copy ** a: ;find first find next calls 
; ** is not a file name we need to find all files that fit that description. We dont need this for our project

;Now we are learning about compression which probably should be a corse of its own. 
;We will be implementing one of the algortinms for sour sedoncd and final project
;A compresion scheme would take files and comvert them to smaller files.
;We want to make sure that we can get all of the infomration from our original files 
; f: files -> files  we want to make sure that our compression is one to one
; ||f(file)|| < ||file|| 
; f is a function 
; this is not possibe for it to be one to one because there are more files of a larger size then there
; are of a smaller size

; so we only want to do this for specific files. For example we cannot compress completely random files
; we can only compress files which have some structure

; First file compression algorithm RLE ..non random files tend to have stretches of the same char
; also for images stretches can be arrays of the same color
; for exe files stretches can be for empty arrays

'runs' = repeated char
"_ _ _ _ _ x++;"

g     same as...
|
7     5,"",-4,"x++;"

;we using 5 to represent the numner of spaces in the string. -4 says that we cannot compress anything else

; Seconds example is Huffman Coding which was created in 1952
; not the best algorithm in terms of compressions 
; very good for how easy it works.
; Based on non random character distribution

; a long time before that there was somthing called morse codes 


; Third example called LZW repeated strings 
