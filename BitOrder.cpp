#include <iostream>

using namespace std;

/*
bool is_big_endian(void)
{
    union {
        uint32_t i;
        char c[4];
    } bint = {0x01020304};

    return bint.c[0] == 1; 
}
*/


/*

Union is a way of specifying a location in memory that can hold different data types
The data types can be of different size 
The endiadiness of the system effects how the smaller data types line up in memory
If theres some crap in the memory you can take it out using a smaller data type
*/

int main()
{
	//cout << is_big_endian() << endl;
	
	union{
		uint32_t i;
		char c[4];
	} bET = {0x01020304};
	
	if (bET.c[0] == 4) cout << "Big End";
	else cout << "Small End";
	
	
}